package event;

import android.os.Bundle;

import base.BaseFragment;


/**
 * Created by Administrator on 2015/11/29.
 */
public class NavFragmentEvent {
    public BaseFragment fragment;
    public Bundle bundle;

    public NavFragmentEvent(BaseFragment fragment) {
        this.fragment = fragment;
    }

    public NavFragmentEvent(BaseFragment fragment, Bundle bundle) {
        this.fragment = fragment;
        this.bundle = bundle;
    }
}
