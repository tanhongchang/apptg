package fragment;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.island.apptg.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import base.BaseFragment;
import widget.QuickFragmentTabHost;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-08
 * Time:10:08
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */

@ContentView(R.layout.fragment_main_tab)
public class MaTabFragment extends BaseFragment implements TabHost.OnTabChangeListener {


    @ViewInject(android.R.id.tabhost)
    private QuickFragmentTabHost mQuickFragmentTabHost;


    private final String[] TITLS = {"首页", "订单", "我的"};
    private final String[] TADS = {"homepage", "orderform", "me"};
    private int ICON[] = {R.drawable.homepager_selector, R.drawable.orderform_selector, R.drawable.me_selector};
    private final Class[] fragments = {HomePageFragment.class, OrderFormFragment.class, MeFragment.class};
    private List<ViewHoler> mTabHoler;

    @Override
    protected void init() {

        mTabHoler = new ArrayList<ViewHoler>();
        mQuickFragmentTabHost.setup(getContext(), getChildFragmentManager(), R.id.real_tabcontent);
        mQuickFragmentTabHost.setOnTabChangedListener(this);

        intiTabs(); //初始化底部tabs

    }

    private void intiTabs() {


        for (int i = 0; i < TITLS.length; i++) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_homepager_icon_view, null);
            ViewHoler viewHoler = new ViewHoler();
            viewHoler.ivIcon = (ImageView) view.findViewById(R.id.iv_tab_icon);
            viewHoler.tvTab = (TextView) view.findViewById(R.id.tv_tab_icon);

            viewHoler.tab = TADS[i];
            mTabHoler.add(viewHoler);
            Drawable drawable = getResources().getDrawable(ICON[i]);
            drawable.setBounds(0, 0, 50, 50);
            viewHoler.tvTab.setCompoundDrawables(null, drawable, null, null);
            viewHoler.tvTab.setText(TITLS[i]);
            mQuickFragmentTabHost.addTab(mQuickFragmentTabHost.newTabSpec(viewHoler.tab).setIndicator(view), fragments[i], null);

        }


    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void onTabChanged(String tabId) {
        Log.i("sssonTabChanged", tabId);
    }


    class ViewHoler {

        ImageView ivIcon;
        TextView tvTab;
        String tab;


    }


}
