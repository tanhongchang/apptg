package fragment;

import com.example.island.apptg.R;

import org.xutils.view.annotation.ContentView;

import base.BaseFragment;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-13
 * Time:15:10
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */

@ContentView(R.layout.fragment_mobile_register)
public class RegisterDetailedFragment extends BaseFragment {
    @Override
    protected void init() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }
}
