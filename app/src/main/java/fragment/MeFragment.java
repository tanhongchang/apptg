package fragment;

import com.example.island.apptg.R;

import org.xutils.view.annotation.ContentView;

import base.BaseFragment;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-08
 * Time:10:38
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 * 我的页面
 */
@ContentView(R.layout.fragment_me)
public class MeFragment extends BaseFragment {
    @Override
    protected void init() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }
}
