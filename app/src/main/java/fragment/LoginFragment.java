package fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.island.apptg.R;

import org.greenrobot.eventbus.EventBus;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import base.BaseFragment;
import event.NavFragmentEvent;
import utils.PopupWindowUtils;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-11
 * Time:14:38
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */

@ContentView(R.layout.fragment_login)
public class LoginFragment extends BaseFragment {


    private PopupWindow popupWindow;
    @ViewInject(R.id.but_Pop)
    private Button button;


    @ViewInject(R.id.tv_fastregistration)
    private TextView tvfastregistration; //快速登录

    private TextView tvMobileRegistration; //手机注册
    private TextView tvMailboxRegister;//邮箱注册


    private TextView tvCancel;

    @Override
    protected void init() {


    }

    @Override
    protected void initListener() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        tvfastregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPop();
            }
        });

    }

    @Override
    protected void initData() {


    }


    public void showPop() {
//        /**
//         * 获取屏幕宽度
//         */
//          DisplayMetrics metrics = new DisplayMetrics();
//          getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//          int w = metrics.heightPixels;

        View popView = LayoutInflater.from(getContext()).inflate(R.layout.popwindow_register, null);
        tvCancel = (TextView) popView.findViewById(R.id.tv_cancle);
        tvMobileRegistration = (TextView) popView.findViewById(R.id.tv_mobile_registration);
        setButtonClickListener();
        PopupWindowUtils.showRegisterPopWindow(popView, getView(), 0);
    }

    //popwindw上面的按钮监听
    public void setButtonClickListener() {
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindowUtils.getMPopupWindow().dismiss();
            }
        });
        tvMobileRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new NavFragmentEvent(new RegisterDetailedFragment()));
                PopupWindowUtils.getMPopupWindow().dismiss();
            }
        });


        tvMailboxRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("Mailboxregister", "");
                EventBus.getDefault().post(new NavFragmentEvent(new RegisterDetailedFragment()));

            }
        });

    }


}

