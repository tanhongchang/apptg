package fragment;

import android.view.View;
import android.widget.Button;

import com.example.island.apptg.R;

import org.greenrobot.eventbus.EventBus;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.Timer;

import base.BaseFragment;
import event.NavFragmentEvent;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-07
 * Time:9:51
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */

@ContentView(R.layout.frament_welcomtent)
public class WelcometFragment extends BaseFragment {

    @ViewInject(R.id.butte)
    private Button button;

    @ViewInject(R.id.zy_button)
    private Button button2;

    private Timer timer;
    @Override
    protected void init() {


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new NavFragmentEvent(new LoginFragment()));

            }
        });


        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new NavFragmentEvent(new MaTabFragment()));
            }
        });


//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                EventBus.getDefault().post(new NavFragmentEvent(new MaTabFragment()));
//            }
//        }, 3000);




    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {


    }

    @Override
    public boolean finish() {
        return true;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
