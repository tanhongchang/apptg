package fragment;

import com.example.island.apptg.R;

import org.greenrobot.eventbus.EventBus;
import org.xutils.view.annotation.ContentView;

import java.util.Timer;
import java.util.TimerTask;

import base.BaseFragment;
import event.NavFragmentEvent;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-07
 * Time:9:54
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */
@ContentView(R.layout.fragment_splash)
public class SplashFragment extends BaseFragment {
    private Timer mTimer;

    @Override
    protected void init() {

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                EventBus.getDefault().post(new NavFragmentEvent(new MaTabFragment()));

            }
        }, 3000);

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public boolean finish() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTimer != null) {

            mTimer.cancel();
            mTimer = null;
        }
    }
}
