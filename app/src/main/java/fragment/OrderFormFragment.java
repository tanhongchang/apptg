package fragment;

import com.example.island.apptg.R;

import org.xutils.view.annotation.ContentView;

import base.BaseFragment;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-08
 * Time:10:37
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 * 订单页
 */
@ContentView(R.layout.fragment_orderform)
public class OrderFormFragment extends BaseFragment {
    @Override
    protected void init() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }
}
