package com.example.island.apptg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-08
 * Time:13:49
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */
public class WecomeActivity extends Activity {

    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        intentInti();
    }

    private void intentInti() {


        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Intent intent = new Intent(WecomeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }, 3000);


        finish();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {

            timer.cancel();
        }
    }
}
