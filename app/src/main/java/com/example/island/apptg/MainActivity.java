package com.example.island.apptg;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.LinkedList;

import base.BaseFragment;
import event.NavFragmentEvent;
import fragment.SplashFragment;
import fragment.WelcometFragment;
import utils.AppUtils;
import utils.ToastUtlis;

public class MainActivity extends AppCompatActivity {


    private FragmentManager fm;


    //储存Fragment的集合
    private LinkedList<String> mFragments = new LinkedList<>();

    public static final int LAST_CLICK_GAP = 600;

    public long lastClickTime = 0;

    private long mExitTime = 0;

    public static final int EXIT_TIME_GAP = 2000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);

        fm = getSupportFragmentManager();


        BaseFragment mBaseFragment;
        String tab;


        //判断是否第一次登陆
        if (AppUtils.isFirstLogin()) {

            mBaseFragment = new SplashFragment();
            tab = mBaseFragment.getMTag();
            AppUtils.firstLogin();//设置为已登陆

        } else {

            mBaseFragment = new WelcometFragment();

            tab = mBaseFragment.getMTag();
        }

        mFragments.add(tab);

        //把FragmentLayout添加到FragmentManager中
        fm.beginTransaction().add(R.id.main_container, mBaseFragment, tab).addToBackStack(tab).commit();




    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (KeyEvent.KEYCODE_BACK == keyCode) {// 返回键处理
            if (!backCurrentFragment()) {
                goBack();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    // 处理默认返回的
    private void goBack() {

        int count = fm.getBackStackEntryCount();
        if (count == 1) {
            // 只存在一个Fragment时候，进行二次提醒
            if (SystemClock.uptimeMillis() - mExitTime > EXIT_TIME_GAP) {
                ToastUtlis.showToastInUIThread("再按一次，退出本程序");
                mExitTime = SystemClock.uptimeMillis();
            } else {
                // 假如多个Activity，清除多个Activity
                MainActivity.this.finish();
            }
        } else {
            if (mFragments.size() > 0) {
                mFragments.pollLast();
            }
            fm.popBackStack();// 把Framgent移除出去
        }
    }


    //Fragment控制返回键
    public boolean backCurrentFragment() {
        BaseFragment currFragment = getCurrentFrament();
        if (currFragment != null) {
            return currFragment.onBack();
        }
        return false;
    }

    @Subscribe
    public void onEventMainThread(NavFragmentEvent event) {

        BaseFragment baseFragment = event.fragment;
        Bundle bundle = event.bundle;
        startFragment(baseFragment, bundle);

    }

    public void startFragment(BaseFragment fragment, Bundle bundle) {
        if (fragment == null) {
            throw new IllegalArgumentException("fragment is null");
        }
        if (lastClickTime + LAST_CLICK_GAP < SystemClock.uptimeMillis()) {
            // 1 获取tag
            String tag = fragment.getMTag();
            // 2 获取事务
            FragmentTransaction ft = fm.beginTransaction();
            // 3 控制Fragment 的动画
//            ft.setCustomAnimations(R.anim.slide_left_enter, 0, 0,R.anim.slide_right_exit);
            // 4 添加Fragment
            ft.add(R.id.main_container, fragment, tag);
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            // 5 隐藏当前或者finish的Fragment
            BaseFragment currFragment = getCurrentFrament();
            if (currFragment != null) {
                if (currFragment.finish()) {
                    mFragments.pollLast();
                    fm.popBackStack();//finish
//                    //由于当前的Fragment，被弹出去，需要当前的Fragment已经变化角色，需要重新隐藏
                    currFragment = (BaseFragment) getCurrentFrament();
                    if (currFragment != null) {
                        ft.hide(currFragment);
                    }
                } else {
                    ft.hide(currFragment);// hide
                }
            }
            // 6 把tag 添加到mFragments
            mFragments.add(tag);
            // 7 添加到返回栈
            ft.addToBackStack(tag);
            // 8 添加事务
            ft.commit();
            lastClickTime = SystemClock.uptimeMillis();
        }
    }

    //获取当前的Fragment
    public BaseFragment getCurrentFrament() {

        return mFragments.size() > 0 ? (BaseFragment) fm.findFragmentByTag(mFragments.peekLast()) : null;
    }
}
