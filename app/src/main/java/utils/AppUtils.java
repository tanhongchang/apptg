package utils;

import android.content.Context;

import base.BaseApplication;


/**
 * Created by Administrator on 2016/2/21.
 */
public class AppUtils {
    public static final String FIRSTLOGIN = "firstLogin";

    public static Context getContext() {
        return BaseApplication.context;
    }

    public static boolean isFirstLogin() {
        return !PreferencesUtils.getInstance().getBoolean(getContext(), FIRSTLOGIN);
    }

    public static void firstLogin() {
        PreferencesUtils.getInstance().putBoolean(AppUtils.getContext(), FIRSTLOGIN, true);
    }
}
