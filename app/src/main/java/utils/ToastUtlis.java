package utils;

import android.widget.Toast;

/**
 * Created by Administrator on 2015/11/3.
 */
public class ToastUtlis {

    public static void showToastInUIThread(String content) {
        Toast.makeText(AppUtils.getContext(), content, Toast.LENGTH_SHORT).show();
    }


    public static void showToastInThread(final String content) {
        ThreadPoolUtils.runTaskInUIThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AppUtils.getContext(), content, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
