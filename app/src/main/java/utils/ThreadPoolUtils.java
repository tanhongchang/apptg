package utils;


import android.os.Handler;

/**
 * Created by Administrator on 2015/11/3.
 */
public class ThreadPoolUtils {
    // 在非UI线程中执行
    public static void runTaskInThread(Runnable task) {
//        new Thread(task).start();
// 线程池
        ThreadPoolFactory.getCommonThreadPool().execute(task);
    }

    // 在UI线程中执行
    public static Handler handler = new Handler();

    public static void runTaskInUIThread(Runnable task) {
        handler.post(task);
    }
}

