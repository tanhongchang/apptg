package utils;

import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

/**
 * AppTg
 * Description:
 * Created by L.Y
 * Date:2016-04-12
 * Time:14:30
 * Copyright :copyright: 2016年 广州火鹰信息科技有限公司. All rights reserved.
 */
public class PopupWindowUtils {


    private static PopupWindow mPopupWindow = new PopupWindow();

    private static PopupWindowUtils popupWindowUtils;


    /**
     * 初始化Popupwindow
     *
     * @return
     */
    public static PopupWindow getMPopupWindow() {

        return mPopupWindow;
//
    }


    public static void showRegisterPopWindow(View view, View group, int w) {
        view.getBackground().setAlpha(80); //设置透明色
        mPopupWindow.setContentView(view);
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
        mPopupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        mPopupWindow.setHeight(w > 0 ? w / 2 : WindowManager.LayoutParams.MATCH_PARENT);
        mPopupWindow.setFocusable(true);
        mPopupWindow.showAtLocation(group, Gravity.BOTTOM, 0, 0);

    }

}
