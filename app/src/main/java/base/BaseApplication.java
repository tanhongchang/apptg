package base;

import android.app.Application;
import android.content.Context;

import org.xutils.BuildConfig;
import org.xutils.x;

/**
 * Created by Administrator on 2016/2/21.
 */
public class BaseApplication extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        // xutil3 注册

        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG);
    }
}
