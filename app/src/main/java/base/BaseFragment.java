package base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.xutils.x;


/**
 * Created by Administrator on 2016/2/21.
 */
public abstract class BaseFragment extends Fragment {
    protected View view;

    private int fId;

    public BaseFragment() {
        fId++;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // x utils 的注入

        view = x.view().inject(this, inflater, container);


        init();
        onGetBundle(getArguments());
        initListener();
        return view;
    }


    public String getMTag() {
        return this.getClass().getName() + fId;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initMState();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    // 必须实现，但要不知道怎么实现，给子类
    protected abstract void init();

    protected abstract void initListener();

    protected abstract void initData();

    // 子类初始化一些状态
    protected void initMState() {

    }

    // 可以获取上一个Fragment传过来的数据
    protected void onGetBundle(Bundle bundle) {

    }

    public View getRootView() {
        return view;
    }

    //把控制返回键的权利交给了Fragment
    public boolean onBack() {
        return false;
    }

    // 子类可以控制生命
    public boolean finish() {
        return false;
    }
}
